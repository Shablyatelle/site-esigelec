<?php
	include_once 'Commun/header.php';
?>
			
		
			<!-- Conteneur principale-->	
			<div class="container-fluid">
				<div class="row">
					
					<div class="col-md-10 col-xs-2 offset-md-1 text-white borderbox">
					
						<!-- Titre-->	
						
						<div class="col-10 offset-1 bg-dark text-warning">
							<p class="titlebox font-weight-bold text-center">Présentation de la filière classique</p>
						</div>
						

							<div>
							  <img src="Images/Amphi-car.jpg"  class="d-block w-100" alt="...">
							</div>

						
						<!-- Texte-->
						
						<div class="col-10 offset-1  text-dark">
							<p class="textbox font-weight-normal">Le Cycle Ingénieur de l’ESIGELEC, en filière classique ou en apprentissage, a pour objectif de vous fournir des compétences généralistes de haut 
							niveau dans les domaines des Systèmes Intelligents et Connectés, mais aussi managériales, linguistiques, éthiques et de professionnalisme, indispensables à un-e ingénieur-e généraliste. 
							De multiples choix vous permettront de construire votre propre parcours, adapté à votre projet personnel et professionnel. L’ESIGELEC c’est une formation équilibrée entre acquisition des 
							technologies, formation humaine et développement du potentiel personnel qui s’appuie notamment sur une pédagogie par projets, un lien étroit avec les entreprises, une forte 
							internationalisation, et une vie associative très développée.</p>
						</div>
						
						<div class="col-10 offset-1  text-dark">
							<p class="textbox font-weight-normal">Vous pouvez voir les cours qui les
							élèves suivent via le lien suivant :</p>
						</div>
						
						<!-- Lien vers Départements-->
						
						<div class="col-10 offset-1  text-dark">
							<a class="nav-link link text-center" href="Departements.php">Départements</a>
						</div>
						
					</div>
					
				</div>
			</div>

			
			  
						

	
	
	
<?php
	include_once 'Commun/footer.php';
?>
