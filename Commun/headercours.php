<?php
	session_start();
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<link rel="stylesheet" href="../Css/style.css">
    <title>Esigelec</title>
	
  </head>
  
<body style="background-color:#adb5bd;">
	
 
				<!-- Navbar ( A mettre en small et big) -->
				<nav class="navbar navbar-expand-lg navbar navbar-dark bg-primary">
			  <div class="container-fluid">
				<a class="navbar-brand" href="../index.php"><img class="logo" src="../Images/Esig-logo.png" alt="Logo Esigelec"></a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNavDropdown">
				  <ul class="navbar-nav">
					<li class="nav-item">
					  <a class="nav-link active" aria-current="page" href="../index.php">Page d'acceuil</a>
					</li>
					<?php
						if (isset($_SESSION['idUser'])){
							echo"<li class='nav-item'>";
							  echo"<a class='nav-link' href='../Php/Deconnexion.php'>Deconnexion</a>";
							echo"</li>";
						}
						else{
							echo"<li class='nav-item'>";
							  echo"<a class='nav-link' href='../Connexion.php'>Connexion</a>";
							echo"</li>";
							echo"<li class='nav-item'>";
							  echo"<a class='nav-link' href='../Inscription.php'>Inscription</a>";
							echo"</li>";
						}?>
					
					<li class="nav-item">
					  <a class="nav-link" href="../Classique.php">Filière Classique</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="../Departements.php">Départements</a>
					</li>
					<li class="nav-item">
					<li class="nav-item dropdown">
					  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						Cours et Sujets
					  </a>
					  <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<li><a class="dropdown-item" href="Departement.php?dep=TIC">TIC</a></li>
						<li><a class="dropdown-item" href="Departement.php?dep=SEI">SEI</a></li>
						<li><a class="dropdown-item" href="Departement.php?dep=ET">ET</a></li>
						<li><a class="dropdown-item" href="Departement.php?dep=HLG">HLG</a></li>
						<li><a class="dropdown-item" href="Departement.php?dep=GEE">GEE</a></li>
					  </ul>
					</li>
				  </ul>
				</div>
			  </div>
			</nav>
			
			<!-- Image du site en longueur-->
		 <div class="container-fluid">

				<div class="row">
					<img src="../Images/esigelec.jpg" height="300" style="padding: 10px" alt="esigelec">
				</div>

		</div>