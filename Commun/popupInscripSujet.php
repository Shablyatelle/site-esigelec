<?php
	include_once '../Commun/headercours.php';
	include_once '../Php/DBhData.php';
	
	$dep = $_GET['dep'];
?>
		
		<!-- Conteneur principale-->
			<div class="container-fluid">
				<div class="row">
					
					<div class="col-md-10 col-xs-2 offset-md-1 text-white borderbox">
					
						<!-- Titre de présentation-->	
						
						<div class="col-10 offset-1 bg-dark text-warning">
							<p class="titlebox font-weight-bold text-center" id="Presentation">Cours et sujet du département <?= $dep ?></p>
						</div>

		
			
						<!-- Départements <?= $dep ?>-->
						<div class="container">
							<h1 class="fw-bold text-center Titredepartement" id="<?= $dep ?>"><?= $dep ?></h1>
 							<?php
							if (isset($_SESSION['Fonction']) && $_SESSION['Fonction'] == 2){
									echo"<button class='btn btn-outline-success me-2' id='Sujet' data-bs-toggle='modal' data-bs-target='#staticBackdrop' type='button'>Ajout de sujet</button>";
									
								}
								else{
								}?>
							  <hr class="hrdepartement">
								
						</div>
						<!-- Printer de sujet <?= $dep ?>-->
						<?php
						$sql = "SELECT * FROM sujet where Departement_nomDepartement='$dep';";
						$result = $conn->query($sql);
						$resultCheck=mysqli_num_rows($result);
						if ($resultCheck > 0){
							$sujet=0;
							echo"<div class='container-fluid row justify-content-center'>";
							foreach ($result as $row) {															
								if ($row ['Departement_nomDepartement'] === $dep){
									echo"<div class='col-xl-4 col-lg-10 col-md-10 rounded sujet col-sm-10 col-xs-10  text-white borderboxSujet sujetBox'>";
									
									
									if (isset($_SESSION['Fonction']) && $_SESSION['Fonction'] == 2){
										echo"		<div class='col-10 offset-1 bg-dark text-warning'>";
										echo"			<p class='titlebox font-weight-bold text-center'>{$row["Titre"]} </p>";
										echo"		</div>";
										echo"		<div class='col-10 offset-1  text-dark'>";
										echo"		<p class ='sujetTexte'>Nombre de place restantes : {$row["Nombre_de_place"]}</p>";
										echo"		<p class ='sujetTexte'>ID du sujet : {$row["idSujet"]}</p>";
										echo"		<p class ='sujetTexte'>Résume du sujet : {$row["Resume"]}</p>";

										
										if (is_null($row["PDF"]) === false){
											echo "<p class ='sujetTexte'>Fichier associé au sujet : </p>";
											echo "<a class=' pdfImg' target='_blank' href='{$row["PDF"]}'><img class ='pdfImg' src='../Images/Pdf.png'  alt='Pdf Link'></a>";

										}
									}
									
									else if (isset($_SESSION['Fonction']) && $_SESSION['Fonction'] == 1 && $_SESSION['Departement'] == $dep){
										echo"		<div class='col-10 offset-1 bg-dark text-warning'>";
										echo"			<p class='titlebox font-weight-bold text-center'>{$row["Titre"]} </p>";
										echo"		</div>";
										echo"		<div class='col-10 offset-1  text-dark'>";
										echo"		<p class ='sujetTexte'>Nombre de place restantes : {$row["Nombre_de_place"]}</p>";
										echo"		<p class ='sujetTexte'>ID du sujet : {$row["idSujet"]}</p>";
										echo"		<p class ='sujetTexte'>Résume du sujet : {$row["Resume"]}</p>";

										
										if (is_null($row["PDF"]) === false){
											echo "<p class ='sujetTexte'>Fichier associé au sujet : </p>";
											echo "<a class=' pdfImg' target='_blank' href='{$row["PDF"]}'><img class ='pdfImg' src='../Images/Pdf.png' alt='Pdf Link'></a>";

										}		
									}
									
									else if (isset($_SESSION['Fonction']) && $_SESSION['Fonction'] == 1){
										echo"		<div class='col-10 offset-1 bg-dark text-warning'>";
										echo"			<p class='titlebox font-weight-bold text-center'>{$row["Titre"]} </p>";
										echo"		</div>";
										echo"		<div class='col-10 offset-1  text-dark'>";
										echo"		<p class ='sujetTexte'>ID du sujet : {$row["idSujet"]}</p>";
										echo"		<p class ='sujetTexte'>Résume du sujet : {$row["Resume"]}</p>";

										
										if (is_null($row["PDF"]) === false){
											echo "<p class ='sujetTexte'>Fichier associé au sujet : </p>";
											echo "<a class=' pdfImg' target='_blank' href='{$row["PDF"]}'><img class ='pdfImg' src='../Images/Pdf.png' alt='Pdf Link'></a>";

										}
									}
									
									
									
									else{
										echo"		<div class='col-10 offset-1 bg-dark text-warning'>";
										echo"			<p class='titlebox font-weight-bold text-center'>{$row["Titre"]} </p>";
										echo"		</div>";
										echo"		<div class='col-10 offset-1  text-dark'>";
										echo"		<p class ='sujetTexte'>Résume du sujet : {$row["Resume"]}</p>";

									}
									
									echo"<hr>";
									if (isset($_SESSION['Fonction']) && $_SESSION['Fonction'] == 2){
										echo"<div class='d-grid gap-2 d-flex justify-content-md-center'>";
										if (isset($_SESSION['Fonction']) && $_SESSION['Fonction'] == 2 && $row ['Utilisateur_idUtilisateur'] == $_SESSION['idUser'] ){
											echo"<button class='btn boutonSujet btn-outline-info ' id='Sujet' data-bs-toggle='modal' data-bs-target='#modifSujet' type='button'>Modifier le sujet</button>";
											echo"<button class='btn boutonSujet btn-outline-danger' id='Sujet' data-bs-toggle='modal' data-bs-target='#supprimSujet' type='button'>Supprimer le sujet</button>";
											echo"<button class='btn boutonSujet btn-outline-primary' id='Sujet' data-bs-toggle='modal' data-bs-target='#listeSujet' type='button'>Liste élève</button>";
										}
										echo"</div>";
									}	
									else if (isset($_SESSION['Fonction']) && $_SESSION['Fonction'] == 1 && $_SESSION['Sujet'] != null && $_SESSION['Sujet'] === $row['idSujet']){
										echo"<div class='d-grid gap-2 d-flex justify-content-md-center'>";
										echo"<button class='btn boutonSujet btn-outline-dark' id='Sujet' data-bs-toggle='modal' data-bs-target='#desinscriptionSujet' type='button'>Desinscription au sujet</button>";
										echo"</div>";
									}
									else if(isset($_SESSION['Fonction']) && $_SESSION['Fonction'] == 1 && $_SESSION['Sujet'] == null && $row["Nombre_de_place"] > 0 && $_SESSION['Departement'] == $dep){
									?>
											<form action='../Php/InscriptionSujet.php' method='post' enctype='multipart/form-data'>
														<input type='hidden' class='form-control btn boutonSujet btn-outline-dark' id='id' name='id' placeholder='ID du sujet' value='<?= $row["idSujet"] ?>'>
												


													<button class='btn btn-primary' name='submit' type='submit'>Inscription au sujet</button>
											</form>	

										<? php
echo"</div>";?>
<?php
									}
								}
							echo"		</div>";
							echo"		</div>";
							}


						}
						
						?>
						
						<!-- Code pour l'onglet des sujet -->
							<?php
								include_once '../Commun/popupSujetCrea.php';
								include_once '../Commun/popupModifSujet.php';
								include_once '../Commun/popupSupprimerSujet.php';
								include_once '../Commun/popupListeEleveSujet.php';
								include_once '../Commun/popupDesinscripSujet.php';
								
							?>
					</div>
				</div>
			</div>

						

	
<?php
	include_once '../Commun/footer.php';
?>