<?php
echo"
<div class='modal fade ' id='modifSujet' data-bs-backdrop='static' data-bs-keyboard='false' tabindex='-1' aria-labelledby='modifSujetLabel' aria-hidden='true'>
								<div class='modal-dialog modal-lg'>
									<div class='modal-content'>
									  <div class='modal-header'>
										<h5 class='modal-title text-dark bold' id='staticBackdropLabel'>Formulaire de modification de sujet</h5>
										<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
									  </div>
									  <div class='modal-body'>
									  <!-- Code form création de sujet -->
											<form action='../Php/ModifSujet.php' method='post' enctype='multipart/form-data'>
												<h1 class='text-center modal-dialog text-dark'>Sujet</h1>
												<hr>
													<div class='mb-3 form-group'>
														<input type='title' class='form-control' id='titre' name='titre' placeholder='Titre' required>
													</div>
													<div class='mb-3 form-group'>
														<input type='place' class='form-control' id='place' name='place' placeholder='Nombre de places' required>
													</div>
													<div class='mb-3 form-group'>
														<select class='form-select' id='departement' name='departement'>
															<option selected>Veuillez selectionner votre département</option>
															<option value='TIC'>TIC</option>
															<option value='SEI'>SEI</option>
															<option value='ET'>ET</option>
															<option value='HLG'>HLG</option>
															<option value='GEE'>GEE</option>
														 </select>
													</div>
													<div class='mb-3 form-group'>
														<textarea  type='resume' rows='3' class='form-control' id='resume' name='resume' placeholder='Resumé du sujet'></textarea>
													</div>
													<div class='mb-3 form-group'>
														<label for='file' class='form-label'>Veuillez insérer un fichier ici</label>
														<input class='form-control form-control-sm' id='file' name='file' type='file'>
													</div>
													<div class='mb-3 form-group'>
														<input type='place' class='form-control' id='id' name='id' placeholder='ID du sujet' required>
													</div>
												


												<div class='modal-footer'>
													<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Fermer</button>
													<button class='btn btn-primary' name='submit' type='submit'>Modifier le sujet</button>
												</div>												
											</form>
										
									
									</div>
								  </div>
								</div>
							</div>
";
?>