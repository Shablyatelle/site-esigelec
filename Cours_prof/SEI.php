<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<link rel="stylesheet" href="../Css/style.css">
    <title>Esigelec : Accueil</title>
	
  </head>
  
  <body style="background-color:#adb5bd;">
	
 
				<!-- Navbar ( A mettre en small et big) -->
				<nav class="navbar navbar-expand-lg navbar navbar-dark bg-primary">
			  <div class="container-fluid">
				<a class="navbar-brand" href="../index.php"><img class="logo" src="../Images/Esig-logo.png" alt="Logo Esigelec"></a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNavDropdown">
				  <ul class="navbar-nav">
					<li class="nav-item">
					  <a class="nav-link" href="../index.php">Page d'acceuil</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="../Connexion.php">Connexion</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="../Inscription.php">Inscription</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="../Classique.php" >Filière Classique</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="../Departements.php">Départements</a>
					</li>
					<li class="nav-item dropdown">
					  <a class="nav-link active dropdown-toggle" aria-current="page" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
						Cours et Sujets
					  </a>
					  <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<li><a class="dropdown-item" href="TIC.php">TIC</a></li>
						<li><a class="dropdown-item" href="SEI.php">SEI</a></li>
						<li><a class="dropdown-item" href="ET.php">ET</a></li>
						<li><a class="dropdown-item" href="HLG.php">HLG</a></li>
						<li><a class="dropdown-item" href="GEE.php">GEE</a></li>
					  </ul>
					</li>
				  </ul>
				</div>
			  </div>
			</nav>
			
			<!-- Image du site en longueur-->
		 <div class="container-fluid">

				<div class="row">
					<img src="../Images/esigelec.jpg" height="300" style="padding: 10px" alt="esigelec">
				</div>

		</div>
		
			<!-- Conteneur principale-->
			<div class="container-fluid">
				<div class="row">
					
					<div class="col-md-10 col-xs-2 offset-md-1 text-white borderbox">
					
						<!-- Titre de présentation-->	
						
						<div class="col-10 offset-1 bg-dark text-warning">
							<p class="titlebox font-weight-bold text-center" id="Presentation">Cours et sujet du département SEI</p>
						</div>

						
						<!-- Départements SEI-->
						<div class="container">
							<h1 class="fw-bold text-center Titredepartement" id="SEI">SEI</h1>
							  <hr class="hrdepartement">
							  	
							  
						</div>						
					</div>
				</div>
			</div>

			
			  
						

	
	
	
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script
 
  </body>
</html>
