<?php
	include_once 'Commun/header.php';
?>
			
		
			<!-- Conteneur principale-->
			<div class="container-fluid">
				<div class="row">
					
					<div class="col-md-10 col-xs-2 offset-md-1 text-white borderbox">
					
						<!-- Titre de présentation-->	
						
						<div class="col-10 offset-1 bg-dark text-warning">
							<p class="titlebox font-weight-bold text-center" id="Presentation">Présentation des différents départements</p>
						</div>

						
						<!-- Texte de présentation-->
						
						<div class="col-10 offset-1 text-dark">
							<p class="textbox font-weight-normal">Chez ESIGELEC les départements permettent
							de diviser notre structure éducationnel en 5 parties plus simple à modifier
							et ainsi optimiser l'éducation de nos élèves.<br> Les 5 départements sont :</p>
							
								<div class="d-grid gap-2 d-flex justify-content-md-center">
								
								<a class="departement btn btn-primary btn-sm fw-normal badge bg-primary text-wrap"  style="width: 6rem;height: 2rem;" href="#TIC">
								TIC
								</a>
								
								<a class="departement btn btn-primary btn-sm fw-normal badge bg-primary text-wrap"  style="width: 6rem;height: 2rem;" href="#SEI">
								SEI
								</a>
								
								<a class="departement btn btn-primary btn-sm fw-normal badge bg-primary text-wrap"  style="width: 6rem;height: 2rem;" href="#ET">
								ET
								</a>
								
								<a class="departement btn btn-primary btn-sm fw-normal badge bg-primary text-wrap"  style="width: 6rem;height: 2rem;" href="#HLG">
								HLG
								</a>
								
								<a class="departement btn btn-primary btn-sm fw-normal badge bg-primary text-wrap"  style="width: 6rem;height: 2rem;" href="#GEE">
								GEE
								</a>
								
								</div>
							<br>
						
						</div>
						
						<div class="col-10 offset-1  text-dark">
							<p class="textbox font-weight-normal text-center">&#8593; Vous pouvez cliquer sur les liens ci-dessus
							pour vous transporter à l'endroit correspondant dans la page. &#8593;</p>
						</div>
						
						<!-- Département TIC-->
						<div class="container">
							<h1 class="fw-bold text-center Titredepartement" id="TIC">TIC</h1>
							  <hr class="hrdepartement">
								<!-- Texte Département TIC-->
								<h2 class="Titrepresentationdepartement">Laboratoire d’Informatique</h2>
								
								<p class="textbox font-weight-normal">⚫Permet aux étudiants de développer leurs
								compétences en programmation, de se former aux techniques/outils du domaine des 
								systèmes d’information, d’accéder à internet, de réaliser le projet ingénieur.
								</p>
								
								<p class="textbox font-weight-normal">⚫Matériels : 110 ordinateurs sous Linux et Windows répartis dans 
								6 salles.
								</p>
								
								<p class="textbox font-weight-normal">⚫Environnements de développement et logiciels : Microsoft Visual 
								Studio, Java (JDK, Eclipse, Tomcat , Androïd ADT), Apache/PHP, bases de données (Oracle, Mysql), 
								atelier de génie logiciel (Modelio), outil de virtualisation (Virtual Box),…
								</p>
								
								<h2 class="Titrepresentationdepartement">Laboratoire réseaux</h2>
								
								<p class="textbox font-weight-normal">⚫Permet aux étudiants d’acquérir un savoir-faire dans 
								le déploiement de la sécurité des réseaux et de l’audit sécurité des systèmes d’information.
								</p>	
								
								<p class="textbox font-weight-normal">⚫Matériel :   30 routeurs : 19  Cisco 2801, 4 Cisco 2800, 
								7 Cisco 2600, 27 switchs : 20 Catalyst 3560, 1 Catalyst 3550, 5 Catalyst 2950, 1 Catalyst 1900.
								</p>
								
								<p class="textbox font-weight-normal">⚫Logiciel :    OracleVirtualBox (virtualisation),   
								Checkpoint (déploiement de la sécurité) ,  GNS3, CISCO packetTracer (simulation réseaux),  
								Wireshark (analyse des trames sur les réseaux)…
								</p>
								
								<p class="textbox font-weight-normal">Les dominantes associées à ce département sont :
								</p>
								
								<p>
									<div class="d-grid gap-2 d-flex justify-content-md-center">
											<div class="fw-normal badge bg-info text-dark text-wrap" style="width: 6rem;">
												IA_IR
											</div>
											
											<div class="fw-normal badge bg-info text-dark text-wrap" style="width: 6rem;">
												ISN
											</div>
											
											<div class="fw-normal badge bg-info text-dark text-wrap" style="width: 6rem;">
												CERT
											</div>
											
											<div class="fw-normal badge bg-info text-dark text-wrap" style="width: 6rem;">
												BDTN
											</div>
											
											<div class="fw-normal badge bg-info text-dark text-wrap" style="width: 6rem;">
												IF
											</div>
									</div>
								
								<div class="d-grid gap-2 d-flex justify-content-md-center">
									<!-- Bouton Warp Accueil-->								
									<a class="departement btn btn-primary btn-sm fw-normal badge bg-primary text-wrap"  style="width: 6rem;height: 2rem;" href="#Presentation">
										Retour à la présentation
									</a>
									<!-- Bouton Cours et sujet TIC-->	

									<a class="departement btn btn-primary btn-sm fw-normal badge bg-primary text-wrap"  style="width: 6rem;height: 2rem;" href="Cours/TIC.php">
									Vers les sujets et Cours
									</a>
							  
						</div>
						
						<!-- Départements SEI-->
						<div class="container">
							<h1 class="fw-bold text-center Titredepartement" id="SEI">SEI</h1>
							  <hr class="hrdepartement">
							  
								<p class="textbox font-weight-normal">Le Département a pour vocation le travail sur 
								les systèmes électroniques embarqués (calculateurs), sur la modélisation mécanique et 
								multiphysique, ainsi que sur les problématiques d’instrumentation et de traitement 
								d’images.</p>
								
								<p class="textbox font-weight-normal">⚫4 salles permettent de travailler sur les 
								microprocesseurs, les microcontrôleurs, les processeurs de traitement du signal, 
								les composants logiques programmables, les systèmes d’exploitation temps réel, les 
								communications filaires ou sans fil, l’instrumentation classique et virtuelle.</p>
								
								<p class="textbox font-weight-normal">⚫Équipements : 70 postes de travail, cartes 
								et outils de développement pour ARM Cortex, Coldfire, HCS12, MSP430 et Texas 6713,
								FPGA CycloneII et Spartan, Exécutif temps réel MicroC/OSII, suites logicielles LabVIEW et Matlab. 
								CAO électronique, CAO mécanique CATIA et Solidworks, Chaînes de développement Code Composer Studio, 
								IAR Workbench, Quartus Altera et ISE Xilinx. Modélisation Amesim…</p>
								
								<p class="textbox font-weight-normal">⚫Équipement de tests pour bus CAN, 
								bus USB. Analyseurs logiques, oscilloscopes numériques, structures robotisées
								(Wifibots, drones Parrot…).</p>
								
								<p class="textbox font-weight-normal">Les dominantes associées à ce département sont :
								</p>
								
								<p>
									<div class="d-grid gap-2 d-flex justify-content-md-center">
											<div class="fw-normal badge bg-info text-dark text-wrap" style="width: 6rem;">
												ISE_OC
											</div>
											
											<div class="fw-normal badge bg-info text-dark text-wrap" style="width: 6rem;">
												ISYMED
											</div>
											
											<div class="fw-normal badge bg-info text-dark text-wrap" style="width: 6rem;">
												MTCGE
											</div>
											
											<div class="fw-normal badge bg-info text-dark text-wrap" style="width: 6rem;">
												ISE_VA
											</div>
									</div>
								
								
								<!-- Bouton Warp Accueil-->
								<div class="d-grid gap-2 d-flex justify-content-md-center">	
									<a class="departement btn btn-primary btn-sm fw-normal badge bg-primary text-wrap"  style="width: 6rem;height: 2rem;" href="#Presentation">
										Retour à la présentation
									</a>
								
								
									<!-- Bouton Cours et sujet SEI-->	
									<a class="departement btn btn-primary btn-sm fw-normal badge bg-primary text-wrap"  style="width: 6rem;height: 2rem;" href="Cours/SEI.php">
									Vers les sujets et Cours
									</a>
								</div>
						</div>
						
						<!-- Départements ET-->
						<div class="container">
							<h1 class="fw-bold text-center Titredepartement" id="ET">ET</h1>
							  <hr class="hrdepartement">
								<h2 class="Titrepresentationdepartement">Laboratoire d’Électronique</h2>
								
								<p class="textbox font-weight-normal">⚫4 salles de travaux pratiques 
								permettent d’étudier l’électronique analogique basse fréquence et 
								l’électronique numérique ainsi que de réaliser les projets.
								</p>		
								
								<p class="textbox font-weight-normal">⚫Une salle projets (en libre-service) avec 
								le même équipement que les 4 salles de travaux pratiques.
								</p>
								
								<p class="textbox font-weight-normal">⚫Équipements : instruments de mesure, 
								maquettes pédagogiques, logiciels de simulation et de circuits imprimés
								</p>
								
								<h2 class="Titrepresentationdepartement">Laboratoire de Télécommunications Hyperfréquences</h2>
								
								<p class="textbox font-weight-normal">⚫Permet aux étudiants d’apprendre
								à utiliser des outils de conception et des appareils de mesures qu’ils rencontreront
								dans l’industrie des télécommunications au cours de leurs stages et de leur 
								carrière professionnelle.
								</p>
								
								<p class="textbox font-weight-normal">⚫Équipements : logiciels de simulation 
								(circuits, électromagnétisme, systèmes), analyseurs vectoriels de réseaux et 
								analyseurs de spectre.
								</p>
								
								<h2 class="Titrepresentationdepartement">Laboratoire de Télécommunications Optiques</h2>
								
								<p class="textbox font-weight-normal">⚫Familiarise les étudiants avec les mesures sur 
								fibres optiques, les manipulations sur maquettes numériques et les simulations 
								logicielles.
								</p>
								
								<p class="textbox font-weight-normal">⚫Équipements : réflectomètre, analyseur de 
								spectre optique, ampli à fibre dopée, logiciels de simulation.
								</p>
								<p class="textbox font-weight-normal">Les dominantes associées à ce département sont :
								</p>
								

									<div class="d-grid gap-2 d-flex justify-content-md-center">
											<div class="fw-normal badge bg-info text-dark text-wrap" style="width: 6rem;">
												ICOM
											</div>
											
											<div class="fw-normal badge bg-info text-dark text-wrap" style="width: 6rem;">
												ESAA
											</div>
									</div>
								
								<!-- Bouton Warp Accueil-->
								<div class="d-grid gap-2 d-flex justify-content-md-center">	
									<a class="departement btn btn-primary btn-sm fw-normal badge bg-primary text-wrap"  style="width: 6rem;height: 2rem;" href="#Presentation">
										Retour à la présentation
									</a>
								
								
									<!-- Bouton Cours et sujet ET-->	
									<a class="departement btn btn-primary btn-sm fw-normal badge bg-primary text-wrap"  style="width: 6rem;height: 2rem;" href="Cours/ET.php">
									Vers les sujets et Cours
									</a>
								</div>
						</div>
						
						<!-- Départements HLG-->
						<div class="container">
							<h1 class="fw-bold text-center Titredepartement" id="HLG">HLG</h1>
							  <hr class="hrdepartement">
							  
								<h2 class="Titrepresentationdepartement">Humanités et Gestion</h2>
								
								<p class="textbox font-weight-normal">L’ESIGELEC a le souci d’offrir à ses étudiants 
								la formation la plus complète possible en l’étendant aux domaines non technologiques pour
								25 % du cursus.
								</p>		
								
								<p class="textbox font-weight-normal">⚫Les enseignements dans des domaines très variés, 
								notamment en communication, en économie, en droit, en musique, en sociologie, en stratégie
								d’entreprise, en marketing, en gestion financière, en management…
								</p>	
								
								<p class="textbox font-weight-normal">⚫La pratique de la gestion de projets à travers le Projet
								Initiative et Créativité
								</p>
								
								<p class="textbox font-weight-normal">⚫L’accompagnement des élèves pour faciliter leur intégration 
								professionnelle : tremplin pour la vie professionnelle, PPP (Projet personnel et professionnel)
								</p>	
								
								<p class="textbox font-weight-normal">⚫Une découverte des différents métiers de l’ingénieur au 
								cours des Approches Métiers
								</p>
								
								<p class="textbox font-weight-normal">⚫La possibilité de préparer un double diplôme avec Neoma 
								Business School ou Audencia Business School
								</p>	
								
								<p class="textbox font-weight-normal">⚫Une sensibilisation à l’innovation au travers de la Trajectoire 
								Innovation et Entrepreneuriat et à l’entrepreneuriat
								</p>	
								
								<h2 class="Titrepresentationdepartement">Langues</h2>
								
								<p class="textbox font-weight-normal">⚫L’ESIGELEC attache une grande importance à l’étude des langues étrangères. 
								Les professeurs enseignent leur langue maternelle et transmettent également leur culture.
								</p>
								
								<p class="textbox font-weight-normal">⚫L’anglais est obligatoire pendant le cursus. Une équipe d’une quinzaine de 
								professeurs, tous anglophones, assure les cours par groupe de niveau d’une douzaine d’élèves. Un score minimum de 
								785 points au TOEIC conditionne l’obtention du diplôme, selon l’exigence de la CTI. Les étudiants peuvent, notamment, 
								passer cette épreuve à l’ESIGELEC dès leur 2e année de cycle ingénieur.
								</p>
								
								<p class="textbox font-weight-normal">⚫Les cours de seconde langue sont obligatoires (excepté pour les  apprentis), 
								à choisir parmi l’espagnol, l’allemand, le chinois, le japonais… ou l’anglais renforcé si le niveau l’exige.
								</p>	
								
								<p class="textbox font-weight-normal">⚫L’ESIGELEC est dotée d’un laboratoire de langues multimédia en libre-service.
								</p>

								<h2 class="Titrepresentationdepartement">Bibliothèque</h2>
								
								<p class="textbox font-weight-normal">L’ESIGELEC met à disposition de tous ses élèves une bibliothèque informatiquement
								équipée, afin que ces derniers puissent effectuer toutes leurs recherches aussi bien dans les domaines techniques que non techniques.
								La bibliothèque accorde également une place importante à l’ouverture culturelle.
								</p>
								
								<p class="textbox font-weight-normal">⚫Les élèves ont accès à 10 ordinateurs avec connexion internet,
								</p>
								
								<p class="textbox font-weight-normal">⚫à plus de 6300 ouvrages,
								</p>
								
								<p class="textbox font-weight-normal">⚫à 50 revues (mensuels, presse quotidienne, sport, économie…)
								</p>
								<!-- Bouton Warp Accueil-->
								<div class="d-grid gap-2 d-flex justify-content-md-center">	
									<a class="departement btn btn-primary btn-sm fw-normal badge bg-primary text-wrap"  style="width: 6rem;height: 2rem;" href="#Presentation">
										Retour à la présentation
									</a>
								
								
									<!-- Bouton Cours et sujet HLG-->	
									<a class="departement btn btn-primary btn-sm fw-normal badge bg-primary text-wrap"  style="width: 6rem;height: 2rem;" href="Cours/HLG.php">
									Vers les sujets et Cours
									</a>
									
								</div>
						</div>
						
						<!-- Départements GEE-->
						<div class="container">
							<h1 class="fw-bold text-center Titredepartement" id="GEE">GEE</h1>
							  <hr class="hrdepartement">
							  
							  <h2 class="Titrepresentationdepartement">Laboratoire d’Automatique Industrielle</h2>
							  
								<p class="textbox font-weight-normal">Permet aux étudiants d’acquérir des connaissances 
								pratiques en Productique (robotique industrielle, systèmes de vision, automates programmables, 
								réseaux locaux, etc.) et en Automatique (modélisation et identification des procédés industriels,
								régulation des systèmes linéaires/non linéaires, commande par calculateur, diagnostic des systèmes 
								complexes, sûreté de fonctionnement,  etc.).
								</p>	
								

								<p class="textbox font-weight-normal">Principaux équipements : Matlab/Simulink, robot Kuka, caméra CCD,
								automates et réseaux Schneider et Siemens…
								</p>		
								
								<h2 class="Titrepresentationdepartement">Laboratoire d’Électrotechnique</h2>

								<p class="textbox font-weight-normal">⚫Permet aux étudiants d’acquérir des connaissances pratiques sur 
								les procédés industriels faisant appel au courant faible et au courant fort (moteurs électriques, 
								électronique de puissance, convertisseurs, variateurs de vitesse, dimensionnement des installations
								électriques, transport et distribution de l’énergie électrique, etc.).
								</p>

								<p class="textbox font-weight-normal">⚫Permet aux étudiants de modéliser et de simuler les équipements 
								utilisés en électronique de puissance (redresseurs, hacheurs, onduleurs,  etc.)
								</p>
								
								<h2 class="Titrepresentationdepartement">Laboratoire Énergies</h2>
								
								<p class="textbox font-weight-normal">⚫Permet aux étudiants d’acquérir des connaissances pratiques sur les sources 
								d’énergies renouvelables (dimensionnement, intégration dans des sites industriels, etc.).
								</p>
								
								<p class="textbox font-weight-normal">⚫Permet aux étudiants de modéliser et de simuler les  équipements utilisés dans le 
								domaine d’énergie renouvelable (PV, éolienne, alternateurs, etc.).
								</p>
								
								<p class="textbox font-weight-normal">⚫Principaux équipements : pile à combustible, éolienne, panneaux solaires, pompe à chaleur...
								</p>
								
								<p class="textbox font-weight-normal">Les dominantes associées à ce département sont :
								</p>
								
								<p>
									<div class="d-grid gap-2 d-flex justify-content-md-center">
											<div class="fw-normal badge bg-info text-dark text-wrap" style="width: 6rem;">
												ARI
											</div>
											
											<div class="fw-normal badge bg-info text-dark text-wrap" style="width: 6rem;">
												EDD
											</div>
											
											<div class="fw-normal badge bg-info text-dark text-wrap" style="width: 6rem;">
												GET
											</div>
											
											<div class="fw-normal badge bg-info text-dark text-wrap" style="width: 6rem;">
												IA_DES
											</div>
									</div>
									
								<!-- Bouton Warp Accueil-->
								<div class="d-grid gap-2 d-flex justify-content-md-center">	
									<a class="departement btn btn-primary btn-sm fw-normal badge bg-primary text-wrap"  style="width: 6rem;height: 2rem;" href="#Presentation">
										Retour à la présentation
									</a>
								
									<!-- Bouton Cours et sujet GEE-->	
									<a class="departement btn btn-primary btn-sm fw-normal badge bg-primary text-wrap"  style="width: 6rem;height: 2rem;" href="Cours/GEE.php">
									Vers les sujets et Cours
									</a>
									
								</div>
						</div>
						
						
						
						
					</div>
				</div>
			</div>

			
			  
						

	
	
	
<?php
	include_once 'Commun/footer.php';
?>