<?php
	include_once 'Commun/header.php';
?>
		
		<!-- Inscription-->
		<!-- Programmer l'inscription-->

			<div class="container">
					<form action="Php/Inscription.php" method="post">
						<h1 class="text-center">Inscription</h1>
						<p class="text-center">Veuillez remplir ce formulaire afin de vous inscrire :</p>
						<hr>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="nom" name="nom" placeholder="nom" required>
								<label for="nom">Nom</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="prenom" name="prenom" placeholder="prenom" required>
								<label for="prenom">Prénom</label>
							</div>
							<div class="form-floating mb-3">
								<input type="email" class="form-control" id="email" name="email" placeholder="email" required>
								<label for="email">Adresse Mail</label>
							</div>
							<div class="form-floating mb-3">
								<input type="password" class="form-control" id="password1" name="password1" placeholder="Mot de Passe" required>
								<label for="password1">Mot de Passe</label>
							</div>
							<div class="form-floating mb-3">
								<input type="password" class="form-control" id="password2" name="password2" placeholder="Confirmation de Mot de Passe" required>
								<label for="password2">Confirmation du Mot de Passe</label>
							</div>
							<div class="input-group mb-3">
								<select class="form-select" id="departement" name="departement">
									<option selected>Veuillez selectionner votre département</option>
									<option value="TIC">TIC</option>
									<option value="SEI">SEI</option>
									<option value="ET">ET</option>
									<option value="HLG">HLG</option>
									<option value="GEE">GEE</option>
								 </select>
							</div>

					  <div class="form-floating">
						<button class="btn btn-primary" name="submit" type="submit">S'inscrire</button>
						<a class="btn btn-secondary cancelbtn"  href="index.php">
							Annuler
						</a>
					  </div>
					</form>
					 
			</div>			
  
  		

	
	
	
<?php
	include_once 'Commun/footer.php';
?>
