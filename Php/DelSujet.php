<?php
session_start();

if(isset($_POST["submit"])){
	$idUser = $_SESSION['idUser'];
	$idSujet = htmlspecialchars($_POST['id']);
	$departement = $_SESSION['Departement'];
	
	
	require_once 'DBh.php';
	require_once 'Fonction.php';
	
	
		//Fonction permettant de crée l'utilisateur dans la base de données.
		//Check quoi faire avec le departement pour l'id

	
	if ($stmt = $conn->prepare("DELETE FROM sujet WHERE idSujet =$idSujet;
	UPDATE utilisateur SET Sujet_idSujet= null WHERE Sujet_idSujet  =$idSujet ;")) {
		
        
        // Le message est mis dans la session, il est préférable de séparer message normal et message d'erreur.
        if($stmt->execute()) {
            $_SESSION['message'] = "Enregistrement réussi";
			if ($departement == 'ET'){
				header("location: ../Cours/Departement.php?dep=ET");}
				else if ($_SESSION['Departement'] == 'TIC'){
				header("location: ../Cours/Departement.php?dep=TIC");}
				else if ($_SESSION['Departement'] == 'GEE'){
				header("location: ../Cours/Departement.php?dep=GEE");}
				else if ($_SESSION['Departement'] == 'SEI'){
				header("location: ../Cours/Departement.php?dep=SEI");}
				else if ($_SESSION['Departement'] == 'HLG'){
				header("location: ../Cours/Departement.php?dep=HLG");}

        } else {
            $_SESSION['message'] =  "Impossible d'enregistrer";
        }
    }
}
else {
	header("location: ../Inscription.php");
	exit();
}