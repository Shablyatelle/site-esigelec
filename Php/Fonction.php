<?php

//Fonction permettant de vérifier si une des cases est vides ou non lors de l'inscription si des cases sont vides alors message d'érreur

function emptyInputSignup($nom, $prenom, $email, $password1, $password2, $departement) {
	$result;
	if (empty($nom) || empty($prenom) || empty($email) || empty($password1) || empty($password2) || empty($departement)){
		$result = true;
	}
	else {
		$result = false;
	}
	return $result;
}

//Fonction permettant de vérifier si le nom et le prénom comporte seulement des caractères valides sinon message d'érreur
function invalidnom($nom) {
	$result;
	if (!preg_match("/^[a-zA-Z]*$/", $nom)){
		$result = true;
	}
	else {
		$result = false;
	}
	return $result;
}

function invalidprenom($prenom) {
	$result;
	if (!preg_match("/^[a-zA-Z]*$/", $prenom)){
		$result = true;
	}
	else {
		$result = false;
	}
	return $result;
}

//Fonction permettant de vérifier si le mail est belle est bien sous forme de mail sinon message d'érreur

function invalidemail($email) {
	$result;
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$result = true;
	}
	else {
		$result = false;
	}
	return $result;
}
//Fonction permettant de vérifier si les mots de passes correspondent entre eux sinon message d'érreur
function passwordmatch($password1, $password2) {
	$result;
	if ($password1 !== $password2){
		$result = true;
	}
	else {
		$result = false;
	}
	return $result;
}

function emptyInputLogin($mail, $pwd) {
	$result;
	if (empty($mail) || empty($pwd)){
		$result = true;
	}
	else {
		$result = false;
	}
	return $result;
}


