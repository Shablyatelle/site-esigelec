<?php
require_once 'DBh.php';
require_once 'Fonction.php';
if(isset($_POST["submit"])){
	$nom = htmlspecialchars($_POST["nom"]);
	$prenom = htmlspecialchars($_POST["prenom"]);
	$email = htmlspecialchars($_POST["email"]);
	$password1 = htmlspecialchars($_POST["password1"]);
	$password2 = htmlspecialchars($_POST["password2"]);
	$departement = htmlspecialchars($_POST["departement"]);
	$role = 1;
	

	
	//Fonction permettant de vérifier si une des cases est vides ou non lors de l'inscription

	if (emptyInputSignup($nom, $prenom, $email, $password1, $password2, $departement) !== false) {
		header("location: ../Inscription.php?error=emptyinput");
		exit();
	}
	//Fonction permettant de vérifier si le nom et le prénom comporte seulement des caractères valides.

	if (invalidnom($nom) !== false) {
		header("location: ../Inscription.php?error=nominvalid");
		exit();
	}
	//Fonction permettant de vérifier si le nom et le prénom comporte seulement des caractères valides.

	if (invalidprenom($prenom) !== false) {
		header("location: ../Inscription.php?error=prenominvalid");
		exit();
	}
	
	if (invalidemail($email) !== false) {
		header("location: ../Inscription.php?error=emailinvalid");
		exit();
	}
	
	if (passwordmatch($password1, $password2) !== false) {
		header("location: ../Inscription.php?error=passwordsnomatch");
		exit();
	}
	
	
		//Fonction permettant de crée l'utilisateur dans la base de données.
		//Check quoi faire avec le departement pour l'id

	
	if ($stmt = $conn->prepare("INSERT INTO utilisateur(Nom,Prenom,Mot_de_passe,Mail,Departement_nomDepartement,Fonction_idFonction) VALUES (:Nom,:Prenom,MD5(:Mot_de_passe),:Mail,:Departement_nomDepartement,:Fonction_idFonction)")) {
        $stmt->bindParam(':Nom',$nom);
        $stmt->bindParam(':Prenom',$prenom);
        $stmt->bindParam(':Mot_de_passe',$password1);
        $stmt->bindParam(':Mail',$email);
		$stmt->bindParam(':Departement_nomDepartement',$departement);
        $stmt->bindParam(':Fonction_idFonction',$role);
		
        
        // Le message est mis dans la session, il est préférable de séparer message normal et message d'erreur.
        if($stmt->execute()) {
            $_SESSION['message'] = "Enregistrement réussi";

        } else {
            $_SESSION['message'] =  "Impossible d'enregistrer";
        }
		header("location: ../index.php");
    }
}
else {
	header("location: ../Inscription.php");
	exit();
}