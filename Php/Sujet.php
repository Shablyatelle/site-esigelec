<?php
session_start();

if(isset($_POST["submit"])){
	$titre = htmlspecialchars($_POST["titre"]);
	$place = htmlspecialchars($_POST["place"]);
	$departement = htmlspecialchars($_POST["departement"]);
	$resume = htmlspecialchars($_POST["resume"]);
	$pdf = $_FILES["file"];
	$idUser = htmlspecialchars($_SESSION['idUser']);
	
	$fileName = $_FILES['file']['name'];
	$fileTmpName = $_FILES['file']['tmp_name'];
	$fileSize = $_FILES['file']['size'];
	$fileError = $_FILES['file']['error'];
	$fileType = $_FILES['file']['type'];
	
	$fileExt = explode('.', $fileName);
	$fileActualExt = strtolower(end($fileExt));
	
	$allowed = array('pdf');
		if ($fileError ===0){
			if ($fileSize < 300000000)
			{
				$fileNameNew = uniqid('', true).".".$fileActualExt;
				$fileDestination = '../Uploads/'.$fileNameNew;
				move_uploaded_file($fileTmpName, $fileDestination);
			
			}
			else {
				echo"Le fichier que vous avez tenté d'upload est de taille supérieur à ce que le serveur peux supporter.";
			}
		}
		else {
			echo"Il y a eu une erreur lors de l'upload du fichier.";
		}
	if (in_array($fileActualExt, $allowed)){
	
	}
	else {
		echo"Vous ne pouvez upload que des fichiers .PDF";
	}

	
	require_once 'DBh.php';
	require_once 'Fonction.php';
	
	
		//Fonction permettant de crée l'utilisateur dans la base de données.
		//Check quoi faire avec le departement pour l'id

	
	if ($stmt = $conn->prepare("INSERT INTO sujet(Titre,Resume,Nombre_de_place,PDF,Utilisateur_idUtilisateur,Departement_nomDepartement) VALUES (:Titre,:Resume,:Nombre_de_place,:PDF,:Utilisateur_idUtilisateur,:Departement_nomDepartement)")) {
        $stmt->bindParam(':Titre',$titre);
        $stmt->bindParam(':Resume',$resume);
        $stmt->bindParam(':Nombre_de_place',$place);
        $stmt->bindParam(':PDF',$fileDestination);
		$stmt->bindParam(':Utilisateur_idUtilisateur',$idUser);
        $stmt->bindParam(':Departement_nomDepartement',$departement);
		
        
        // Le message est mis dans la session, il est préférable de séparer message normal et message d'erreur.
        if($stmt->execute()) {

			header("location: ../Cours/Departement.php?dep=".$_SESSION['Departement']);

        } else {
            $_SESSION['message'] =  "Impossible d'enregistrer";
        }
    }
}
else {
	//header("location: ../Inscription.php");
	exit();
}