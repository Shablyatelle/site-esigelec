<?php
	include_once 'Commun/header.php';
?>
			
			<!-- Message centrée de bienvenue-->
			
			<div class="container">
				<div class="row">
					 <div class="col-md-4 offset-md-4">
						<h1 class="titre text-center">Bienvenue sur le site d'ESIGELEC</h1>
					 </div>
				</div>
			</div>
				
			<!-- Deux blocs de textes de gauches-->
			<!-- Rajouter du padding et faire en sorte que lors d'un squish il y ait séparation-->
            <div class="container-fluid">
				<div class="row">
					<!-- Colonne 1-->
					<div class="col-md-5 col-xs-2 offset-md-1 text-white borderbox colgo">
					
						<div class="col-10 offset-1 bg-dark text-warning">
							<p class="titlebox font-weight-bold text-center">Bonjour <?php
							if (isset($_SESSION['Nom'])){
							echo "{$_SESSION['Nom']} {$_SESSION['Prenom']}";
							}
							else{}
							?>
							</p>
						</div>

						<div class="col-10 offset-1  text-dark">
							<?php
								if (isset($_SESSION['Fonction']) && $_SESSION['Fonction'] == 1){
									echo"<p class='textbox font-weight-normal'>Vous êtes un étudiant dans l'école d'ingénieur ESIGELEC.</p>";
									echo"<p class='textbox font-weight-normal'>Vous étudier dans le département '{$_SESSION['Departement']}'.</p>";
								}
								else if (isset($_SESSION['Fonction']) && $_SESSION['Fonction'] == 2 ){
									echo"<p class='textbox font-weight-normal'>Vous êtes un professeur dans l'école d'ingénieur ESIGELEC.</p>";
									echo"<p class='textbox font-weight-normal'>Vous travailler dans le département '{$_SESSION['Departement']}'.</p>";
								}
								else{
									echo"<p class='textbox font-weight-normal'>Vous êtes actuellement un visiteur, pour plus d'information veuillez vous connecter.</p>";
								}
							?>
						</div>
							
					</div>
					
					<!-- Colonne 2-->
					
					<div class="col-md-5 col-xs-2 text-white borderbox coldro ">
						<div class="col-10 offset-1 bg-dark text-warning">
							<p class="titlebox font-weight-bold text-center">Actualités</p>
						</div>

						<div class="col-10 offset-1  text-dark">
							<p class="textbox font-weight-normal">Pour des raisons évidentes de sécurité le parc boisé, sur l’ensemble du site ESIGELEC (PEDAGOGIE et RECHERCHE), est interdit d’accès le temps que l’entreprise d’élagage réalise les travaux de sécurisation. Un message vous autorisant de nouveau l’accès vous sera envoyé. Soyez attentifs sur les abords extérieurs de l’école de nombreux déchets végétaux jonchent le sol, les risques de chutes ou glissades sont importants. Les services de la Ville de Saint Etienne du Rouvray sont à pied d’œuvre pour nettoyer les environs. </p>
						</div>
					</div>
					
				</div>
			</div>
					

	
<?php
	include_once 'Commun/footer.php';
?>
