-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 28, 2021 at 12:16 AM
-- Server version: 5.7.11
-- PHP Version: 7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oui`
--

-- --------------------------------------------------------

--
-- Table structure for table `departement`
--

CREATE TABLE `departement` (
  `idDepartement` int(11) NOT NULL,
  `Nom` varchar(45) NOT NULL,
  `Sujet_idSujet` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fonction`
--

CREATE TABLE `fonction` (
  `idFonction` int(11) NOT NULL,
  `Nom` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sujet`
--

CREATE TABLE `sujet` (
  `idSujet` int(11) NOT NULL,
  `Titre` varchar(45) NOT NULL,
  `Resume` text NOT NULL,
  `Nombre_de_place` varchar(45) NOT NULL,
  `PDF` varchar(45) DEFAULT NULL,
  `Utilisateur_idUtilisateur` int(11) NOT NULL,
  `Departement_nomDepartement` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sujet`
--

INSERT INTO `sujet` (`idSujet`, `Titre`, `Resume`, `Nombre_de_place`, `PDF`, `Utilisateur_idUtilisateur`, `Departement_nomDepartement`) VALUES
(6, 'aze', 'sdfdsfdsffds', '118', NULL, 5, 'ET'),
(7, 'azeeeeee', '', '1', NULL, 5, 'ET');

-- --------------------------------------------------------

--
-- Table structure for table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `idUtilisateur` int(11) NOT NULL,
  `Nom` varchar(45) NOT NULL,
  `Prenom` varchar(45) NOT NULL,
  `Mot_de_passe` varchar(45) NOT NULL,
  `Mail` varchar(45) NOT NULL,
  `Sujet_idSujet` int(11) DEFAULT NULL,
  `Fonction_idFonction` int(11) NOT NULL,
  `Departement_nomDepartement` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `utilisateur`
--

INSERT INTO `utilisateur` (`idUtilisateur`, `Nom`, `Prenom`, `Mot_de_passe`, `Mail`, `Sujet_idSujet`, `Fonction_idFonction`, `Departement_nomDepartement`) VALUES
(2, 'aze', 'aze', '0a5b3913cbc9a9092311630e869b4442', 'aze@aze.fr', NULL, 1, 'SEI'),
(3, 'azer', 'azer', '0a5b3913cbc9a9092311630e869b4442', 'azre@aze.fr', 6, 1, 'TIC'),
(4, 'aled', 'aled', '5bc23e2ce9a4c61b4622f4a5edf3d10b', 'aled@aled.fr', 6, 1, 'SEI'),
(5, 'prof', 'prof', 'd450c5dbcc10db0749277efc32f15f9f', 'prof@prof.fr', NULL, 2, 'HLG'),
(6, 'dq', 'dq', '47bcdcd7bcbf990c435227b4aa4912da', 'dq@dq.fr', 6, 1, 'ET');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departement`
--
ALTER TABLE `departement`
  ADD PRIMARY KEY (`idDepartement`);

--
-- Indexes for table `fonction`
--
ALTER TABLE `fonction`
  ADD PRIMARY KEY (`idFonction`);

--
-- Indexes for table `sujet`
--
ALTER TABLE `sujet`
  ADD PRIMARY KEY (`idSujet`);

--
-- Indexes for table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`idUtilisateur`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departement`
--
ALTER TABLE `departement`
  MODIFY `idDepartement` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fonction`
--
ALTER TABLE `fonction`
  MODIFY `idFonction` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sujet`
--
ALTER TABLE `sujet`
  MODIFY `idSujet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `idUtilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
